﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CustomersProtoApp;
using Grpc.Net.Client;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;


namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersGPRCController
        : ControllerBase
    {
        private string _gRPCAddress = "https://localhost:7233";
 
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {

            // создаем канал для обмена сообщениями с сервером
            // параметр - адрес сервера gRPC
            using var channel = GrpcChannel.ForAddress(_gRPCAddress);

            // создаем клиент
            var client = new CustomersProto.CustomersProtoClient(channel);
            var customersProtoGetCustomersRequest = new CustomersProtoGetCustomersRequest();

            var responseContent = await client.CustomersProtoGetCustomersAsync(customersProtoGetCustomersRequest);

            var result = JsonConvert.DeserializeObject<List<CustomerShortResponse>>(responseContent.Data);
             
            return Ok(result);
        }
         
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteCustomerAsync(Guid id)
        {
            // создаем канал для обмена сообщениями с сервером
            // параметр - адрес сервера gRPC
            using var channel = GrpcChannel.ForAddress(_gRPCAddress);

            // создаем клиент
            var client = new CustomersProto.CustomersProtoClient(channel);
            var customersProtoDeleteCustomerRequest = new CustomersProtoApp.CustomersProtoDeleteCustomerRequest() { Id = id.ToString() };
             
            var response = await client.CustomersProtoDeleteCustomerAsync(customersProtoDeleteCustomerRequest);

            if (response.Data == "NotFound") {
                return BadRequest("NotFound");
            }
            return NoContent();
        }
    }
}
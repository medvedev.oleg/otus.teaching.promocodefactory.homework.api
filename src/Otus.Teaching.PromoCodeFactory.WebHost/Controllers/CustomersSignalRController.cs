﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR.Client;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using RouteAttribute = Microsoft.AspNetCore.Components.RouteAttribute;


namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersSignalRController : ControllerBase
    {
        private string _signalRAddress = "https://localhost:7098";

        private HubConnection _connection;  // подключение для взаимодействия с хабом

        public CustomersSignalRController(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            // создаем подключение к хабу
            _connection = new HubConnectionBuilder()
                    .WithUrl($"{_signalRAddress}/customers")
                    .Build();

            // регистрируем функцию DeleteCustomerObserver для удаления пользовпателя
            _connection.On<string>("DeleteCustomerObserver", (message) =>
            {
                var result = new CustomersController(customerRepository, preferenceRepository).DeleteCustomerAsync(Guid.Parse(message));
            });
        }


        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteCustomerAsync(Guid id)
        {
            // подключемся к хабу
            await _connection.StartAsync();

            try
            {
                // отправка сообщения
                await _connection.InvokeAsync("DeleteCustomer", id.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception:", ex);
            }

            await _connection.StopAsync();// отключение от хаба

            return NoContent();
        }
    }
}
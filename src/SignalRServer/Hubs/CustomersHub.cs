using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading.Tasks;

namespace SignalRApp
{
    public class CustomersHub : Hub
    {
     
        public async Task DeleteCustomer(Guid message)
        {
            await this.Clients.All.SendAsync("DeleteCustomerObserver", message);
        }
    }
}
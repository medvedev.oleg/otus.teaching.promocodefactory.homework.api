using CustomersProtoApp;
using Grpc.Core;
using Newtonsoft.Json;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace CustomersServiceApp.Services
{
    public class CustomersService : CustomersProto.CustomersProtoBase
    {
          private readonly IRepository<Customer> _customerRepository;
         private readonly IRepository<Preference> _preferenceRepository;

        public CustomersService(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
       // public CustomersService()
        {
                _customerRepository = customerRepository;
              _preferenceRepository = preferenceRepository;
        }


        public override async Task<CustomersProtoGetCustomersResponse> CustomersProtoGetCustomers(CustomersProtoGetCustomersRequest request, ServerCallContext context)
        {

            // var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));
            var customers = await _customerRepository.GetAllAsync();
            var response = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();
                        
            var result = JsonConvert.SerializeObject(response);

            return await Task.FromResult(new CustomersProtoGetCustomersResponse { Data = result });

        }

      

            


        public override async Task<CustomersProtoDeleteCustomerResponse> CustomersProtoDeleteCustomer(CustomersProtoDeleteCustomerRequest request, ServerCallContext context)
        {

            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            if (customer == null)
                return await Task.FromResult(new CustomersProtoDeleteCustomerResponse { Data = "NotFound" });

            await _customerRepository.DeleteAsync(customer);

            return await Task.FromResult(new CustomersProtoDeleteCustomerResponse { Data = "Success" });

        }
    }
}

